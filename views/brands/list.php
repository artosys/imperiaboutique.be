<?php if ($this->config->item('sys_active_page') == 'pages/home') { ?>

	<?php foreach($brands as $brand){ ?>
		<div class="carousel-item merken-img">
			<a href="{url}merken">
			<?php if(isset($brand->pictures[0])){ ?>
				<img src="<?= SHAREURL."uploads/".WEBSITE_KEYWORD."/".$brand->pictures[0] ?>" class="img-transparency" alt="<?= $brand->name ?>">
			<?php } ?>
			</a>
		</div>
	<?php } ?>

<?php } else { ?>

	<?php foreach($brands as $brand){ ?>
	
		<div class="element isotope-item branding brand-sqr">
			<div class="element-inner animation fadeInLeft">

                <div class="overlay-wrapper">
					<?php if(isset($brand->pictures[0])){ ?>
					<img src="<?= SHAREURL."uploads/".WEBSITE_KEYWORD."/".$brand->pictures[0] ?>" width="1200" height="900" alt="<?= $brand->name ?>" />
					<?php } ?>
                    <div class="overlay-wrapper-content">
						<div class="overlay-title"><h3><?= $brand->description ?></h3></div>
                        <div class="overlay-details">
							<a class="color-white" target="_blank" href="<?= $brand->url ?>"><span class="livicon" data-n="more"  data-color="#ffffff" data-hovercolor="#ffffff" data-op="1" data-onparent="true"></span></a>                                    	
                        </div>
                        <div class="overlay-bg"></div>
					</div>
                </div>										

			</div>
		</div>

	<?php } ?>

<?php } ?>
