<h2><?= $category->name ?></h2>
<div class="row">
	<?php foreach($items as $item){ ?>
	<div class="col-sm-12 col-md-4">
		<a href="{url}items/item/<?= $item->id ?>/<?= urlencode($item->name) ?>" class="item_block">
			<?php if(isset($item->picture)){ ?>
			<img src="<?= SHAREURL."uploads/".WEBSITE_KEYWORD."/".$item->picture ?>" width="300" />
			<?php } ?>
			<div class="name">
				<?= $item->name ?>
			</div>
		</a>
	</div>
	<?php } ?>
	<?php if(count($items) == 0){ ?>
	<div class="col-xs-12">
		Geen producten in deze categorie.
	</div>
	<?php } ?>
</div>