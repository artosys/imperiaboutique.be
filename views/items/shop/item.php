<div class="row">
	<div class="col-xs-12 col-sm-6 pictures_container">
		<?php if(count($pictures) > 0){ ?>
		<div class="master-slider ms-skin-default" id="product_pictures">
			<?php foreach($pictures as $picture){ ?>
			<div class="ms-slide">
				<img src="<?= SHAREURL ?>js/ms/blank.gif" data-src="<?= SHAREURL."uploads/".WEBSITE_KEYWORD."/".$picture->location; ?>" alt="" />
				<img class="ms-thumb" src="<?= SHAREURL."uploads/".WEBSITE_KEYWORD."/".$picture->location; ?>" alt="thumb" />
			</div>
			<?php } ?>
		</div>
		<?php } else { ?>
		Dit product heeft geen afbeeldingen beschikbaar.
		<?php } ?>
	</div>
	<div class="col-xs-12 col-sm-6">
		<h2><?= $item->name ?></h2>
		<?= $item->description ?>
		<div class="row balloons">
			<div class="col-xs-6">
				<div class="balloon">
					<div class="row">
						<div class="col-xs-6">
							Aantal:
						</div>
						<div class="col-xs-6">
							
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6">
				<div class="balloon price">
					€ <?= $item->price ?>
				</div>
			</div>
		</div>
		<a class="btn btn-shop">In winkelwagen</a>
	</div>
</div>

<script>
    var slider = new MasterSlider();
    slider.setup('product_pictures' , {
		layout: 'fillwidth',
		width:468,
		height:303,  
		space:0,
		fillMode: 'fit'
	});
    // adds Arrows navigation control to the slider.
    slider.control('arrows');
	slider.control('thumblist' , {autohide:false ,dir:'h',arrows:false});
</script>