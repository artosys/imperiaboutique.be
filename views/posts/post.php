		<!-- Main Wrapper Header -->
        <div class="main-wrapper-header fancy-header dark-header parallax parallax-blog" data-stellar-background-ratio="0.4">
			
            <div class="bg-overlay bg-overlay-dark"></div>

 			<div class="container">
            
				<div class="row">
                	<div class="col-sm-12 columns">
                		<div class="page-title">
                    		<h2>Nieuws</h2>                    
                    	</div>
                    	<div class="breadcrumbs-wrapper">               
							<ol class="breadcrumb">
  								<li><a href="index.html">Home</a></li>
								<li><a href="nieuws.html">Nieuws</a></li>
                                <li class="active">Item</li>
							</ol>
                		</div>
					</div>
				</div>
                
			</div>
 
        </div>       
		<!-- /Main Wrapper Header -->        

		<!-- Main Container -->
		<div class="main-wrapper">

        	<!-- Container -->
            <div class="container">
            
            	<div class="row">

					<div class="col-sm-12">                    
                    	<div class="white-space space-big"></div>
                        
                    	<!-- Blog Content -->
                        <div class="row">                        
                            <div class="col-sm-12">

								<!-- blog post -->
								<div class="blog-post post-format-gallery">
                                
									<div class="blog-post-side">
                                    	<div class="blog-post-date">
                                        	<p class="date-day">
												<?php 
													$datetime = new DateTime($post->date_created);
													echo $datetime->format('d'); 
												?>
											</p>
                                            <p class="date-month">
												<?php 
												setlocale(LC_TIME, 'nl_NL');	
												$datetime = new DateTime($post->date_created);
													echo substr($datetime->format('F'), 0, 3); 
												?>
											</p>
                                        </div>
                                        
                                    	<a href="{url}posts/like/<?= $post->id ?>">
                                            <div class="nieuws-icon collapsed-icon collapsed ToolTip" title="Vind ik leuk" data-opie-position="mr:ml">
                                                <span class="livicon" data-n="thumbs-up" data-s="36" data-color="#95a252" data-hovercolor="#99B021" data-onparent="true"></span>
                                            </div>
                                        </a> 
                                        <a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>/#post_comments">
                                            <div class="nieuws-icon collapsed-icon collapsed ToolTip" title="Reageren" data-opie-position="mr:ml">
                                                <span class="livicon" data-n="comments" data-s="36" data-color="#95a252" data-hovercolor="#99B021" data-onparent="true"></span>
                                            </div> 
                                        </a>  
                                        <a href="javascript:fbShare('{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>', 520, 350)">
                                            <div class="nieuws-icon collapsed-icon collapsed ToolTip" title="Delen op Facebook" data-opie-position="mr:ml">
                                                <span class="livicon" data-n="redo" data-s="36" data-color="#95a252" data-hovercolor="#99B021" data-onparent="true"></span>
                                            </div> 
                                        </a>                                             
                                    </div>  
                                    
                                    <div class="blog-post-content">
										<?php if ( count($pictures) > 1 ) { ?>
										
										<div class="post-media">
											<div class="carousel-box animation flipInY">
												<div class="carousel" data-carousel-autoplay="6000" data-carousel-nav="true" data-carousel-pagination="false" data-carousel-single="true" data-carousel-transition="goDown">
													<?php foreach($pictures as $picture){ ?>
													<div class="carousel-item "><img src="<?= SHAREURL."uploads/".WEBSITE_KEYWORD."/".$picture->src_original ?>" alt="<?= urlencode($post->title) ?>" width="1225" height="450" style="max-height: 450px;"></div>		
													<?php } ?>
												</div>
											</div>
                                        </div>
										
										<?php } elseif ( count($pictures) > 0 ) { ?>
										
										<div class="post-media">
									    	<img src="<?= SHAREURL."uploads/".WEBSITE_KEYWORD."/".$pictures[0]->src_original ?>" alt="<?= urlencode($post->title) ?>" width="1225" height="450" style="max-height: 450px;" />
                                        </div>
										
										<?php } elseif ( count($post->videos) > 0 ) { if ( strpos($post->videos[0]->url,'v=') !== false ) { $videourl = "https://www.youtube.com/embed/".explode('v=',$post->videos[0]->url)[1]; } else { $videourl = $post->videos[0]->url; } ?>
										
										<div class="post-media animation flipInY">
									    	<div class="flex-video">
            									<iframe src="http://player.vimeo.com/video/23237102?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="1280" height="720" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
												<iframe width="1280" height="720" src="<?= $videourl ?>?rel=0" frameborder="0" allowfullscreen style="border-radius: 4px;"></iframe>
            								</div>
                                        </div>
										
										<?php } ?>
										
										<!--
										<div class="post-media">
									    	<div class="flex-video vimeo">
            									<iframe src="http://player.vimeo.com/video/23237102?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="1280" height="720" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            								</div>
                                        </div>
										-->
										<div class="post-info">
											<h3 class="post-title"><a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>"><?= $post->title ?></a></h3>
										</div>  
										<div class="post-content" style="overflow: hidden;">
											<div style="overflow: hidden;"><?= nl2br($post->text) ?></div>
											
                                            <ul class="list-inline post-meta-info">
												<li>
													<a href="{url}posts/like/<?= $post->id ?>" class="ToolTip" title="Vind ik leuk">
														<span class="livicon" data-name="thumbs-up" data-size="24" data-color="#c0c0ca" data-hovercolor="#99B021"></span> 
														<?= count($likes) ?>
													</a>
												</li>
												<li>
													<a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>/#post_comments" class="ToolTip" title="Reageren">
														<span class="livicon" data-name="comments" data-size="24" data-color="#c0c0ca" data-hovercolor="#99B021"></span> 
														<?= count($comments) ?>
													</a>
												</li>
                                                <li>
													<a href="javascript:fbShare('{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>', 520, 350)" class="ToolTip" title="Delen op Facebook">
														<span class="livicon" data-name="redo" data-size="24" data-color="#c0c0ca" data-hovercolor="#99B021"></span> 
														<?= $post->shared ?>
													</a>
												</li>
											</ul>
                                        </div>
                                    </div> 
                                                        
                				</div>
								<!-- /blog post -->                             
                           
                            </div>
                        </div>
                    	<!-- /Blog Content -->

                    	<?php if($post->allow_comments == 1){ ?>
						<!-- Comments -->
						<?php if(count($comments) > 0){ ?>
                        <div class="row">                        
                            <div class="col-sm-12">
							
                            	<h4 class="fancy-title"><span><?= count($comments) ?> Reactie<?= count($comments) == 1 ? "":"s" ?></span></h4>                         
								<ul class="media-list comments-article">
									<?php foreach($comments as $comment){ ?>
									<li class="media">
										<div class="media-body">
											<h4 class="media-heading"><?= $comment->name ?><span class="comment-date"><?= strftime("%d %B, %Y", strtotime($comment->date_created)) ?></span></h4>
											<div><?= nl2br($comment->comment) ?></div>
										</div>
									</li>
									<?php } ?>
								</ul>
                                                           
                            </div>
                        </div>
						<?php } ?>
                    	<!-- /Comments -->
 
                     	<!-- Leave Comment -->
                        <div class="row" id="post_comments">                        
                            <div class="col-sm-12">

								<h4 class="fancy-title"><span>Plaats een reactie</span></h4> 
								<div class="error" id="comment_error" style="display: none;"></div>
                        		<!-- Form -->
								<form method="post" action="{url}posts/comment/<?= $post->id ?>" id="post_comment">
									<div class="form-group">
										<div class="col-sm-6">
											<input type="name" class="form-control" name="name" id="comment_name" placeholder="Naam">
										</div>
										<div class="col-sm-6">
											<input type="email" class="form-control" name="email" id="comment_email" placeholder="E-mail">
										</div>   
										<div class="col-sm-12">
											<textarea name="comment" id="comment_comment" class="form-control" rows="5" placeholder="Uw reactie"></textarea>
										</div> 
										<div class="col-sm-12 text-center">
											<input type="submit" name="btn_comment" class="btn btn-primary" value="Reactie toevoegen" />
										</div>
									</div>
								</form>                        
                        		<!-- /Form -->
                                <div class="white-space space-medium"></div>

                            </div>                                                  
                        </div>
                    	<!-- /Leave Comment -->
						<?php } ?>
						
                        <div class="white-space space-big"></div>
                    </div>
                
                </div>          
            	
            </div>
			<!-- /Container -->
                                   
		</div>
		<!-- /Main Container -->
		
		<script type="text/javascript">
			$(document).ready(function(){
				$("#post_comment").submit(function(e) {
					comment_valid = true;
					$("#comment_error").text('');
					if($.trim($("#comment_name").val()) == "")
					{
						comment_valid = false;
					}
					var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
					if($.trim($("#comment_email").val()) == "" || !re.test($("#comment_email").val()))
					{
						comment_valid = false;
					}
					if($.trim($("#comment_comment").val()) == "")
					{
						comment_valid = false;
					}
					if(comment_valid == false)
					{
						e.preventDefault();
						$("#comment_error").text('Gelieve alle velden correct en volledig in te vullen.');
						$("#comment_error").show();
					}
				});
			});
			function fbShare(url, winWidth, winHeight) {
				var winTop = (screen.height / 2) - (winHeight / 2);
				var winLeft = (screen.width / 2) - (winWidth / 2);
				window.open('http://www.facebook.com/sharer.php?s=100&p[url]=' + url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
			}
		</script>