<?php if ($this->config->item('sys_active_page') == 'pages/home') { ?>
<?php foreach($posts as $post){ ?>

			<div class="carousel-item">
                <a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>">
					<?php if ( count($post->pictures) > 0 ) { ?>
					<img src="<?= SHAREURL."uploads/".WEBSITE_KEYWORD."/".$post->pictures[0]->src_original ?>" alt="<?= urlencode($post->title) ?>" style="width: 295px; height: 166px;">
					<?php } elseif ( count($post->videos) > 0 ) { if ( strpos($post->videos[0]->url,'v=') !== false ) { $videourl = "https://www.youtube.com/embed/".explode('v=',$post->videos[0]->url)[1]; } else { $videourl = $post->videos[0]->url; } ?>
					<iframe width="295" height="166" src="<?= $videourl ?>?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen style="border-radius: 4px;"></iframe>
					<?php } ?>
				</a>
                <div class="panel panel-default donkerder-container">
                    <div class="panel-body donkerder">
                       <h5><a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>"><?= substr($post->title, 0, 25) ?></a></h5>
                       <div style="height: 50px; overflow: hidden;"><?= nl2br(substr($post->text, 0, 80)) ?></div>
                           <ul class="list-inline post-meta-info">
								<li>
									<a href="{url}posts/like/<?= $post->id ?>">
										<span class="livicon" data-name="thumbs-up" data-size="16" data-color="#c0c0ca" data-hovercolor="#99B021"></span> 
										<?= $post->likes ?>
									</a>
								</li>
            					<li>
									<a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>/#post_comments">
										<span class="livicon" data-name="comments" data-size="16" data-color="#c0c0ca" data-hovercolor="#99B021">
											
										</span> 
										<?= $post->comments ?>
									</a>
								</li>
                                <li>
									<a href="javascript:fbShare('{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>', 520, 350)">
										<span class="livicon" data-name="redo" data-size="16" data-color="#c0c0ca" data-hovercolor="#99B021"></span> 
										<?= $post->shared ?>
									</a>
								</li>
                                <li style="float: right;"><?= $post->date_elapsed ?></li>
						   </ul>
                    </div>
                </div>
             </div>

<?php } ?>
<?php } else { ?>

<!-- /Blog Content -->
<div class="row">                        
<div class="col-sm-12">
	<?php foreach($posts as $post){ ?>

								<div class="blog-post post-format-gallery">

										<div class="blog-post-side">
											<div class="blog-post-date">
												<p class="date-day">
													<?php 
														$datetime = new DateTime($post->date_created);
														echo $datetime->format('d'); 
													?>
												</p>
												<p class="date-month">
													<?php 
													setlocale(LC_TIME, 'nl_NL');	
													$datetime = new DateTime($post->date_created);
														echo substr($datetime->format('F'), 0, 3); 
													?>
												</p>
											</div>

											<a href="{url}posts/like/<?= $post->id ?>">
												<div class="nieuws-icon collapsed-icon collapsed ToolTip" title="Vind ik leuk" data-opie-position="mr:ml">
													<span class="livicon" data-n="thumbs-up" data-s="36" data-color="#95a252" data-hovercolor="#99B021" data-onparent="true"></span>
												</div>
											</a> 
											<a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>/#post_comments">
												<div class="nieuws-icon collapsed-icon collapsed ToolTip" title="Reageren" data-opie-position="mr:ml">
													<span class="livicon" data-n="comments" data-s="36" data-color="#95a252" data-hovercolor="#99B021" data-onparent="true"></span>
												</div> 
											</a>  
											<a href="javascript:fbShare('{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>', 520, 350)">
												<div class="nieuws-icon collapsed-icon collapsed ToolTip" title="Delen op Facebook" data-opie-position="mr:ml">
													<span class="livicon" data-n="redo" data-s="36" data-color="#95a252" data-hovercolor="#99B021" data-onparent="true"></span>
												</div> 
											</a>                                        
										</div>  

										<div class="blog-post-content">

											<?php if ( count($post->pictures) > 1 ) { ?>

											<div class="post-media">
												<div class="carousel-box animation flipInY">
													<div class="carousel" data-carousel-autoplay="6000" data-carousel-nav="true" data-carousel-pagination="false" data-carousel-single="true" data-carousel-transition="goDown">
														<?php foreach($post->pictures as $picture){ ?>
														<div class="carousel-item "><img src="<?= SHAREURL."uploads/".WEBSITE_KEYWORD."/".$picture->src_original ?>" alt="<?= urlencode($post->title) ?>" width="1225" height="450" style="max-height: 450px;"></div>		
														<?php } ?>
													</div>
												</div>
											</div>

											<?php } elseif ( count($post->pictures) > 0 ) { ?>

											<div class="post-media animation flipInY">
												<img src="<?= SHAREURL."uploads/".WEBSITE_KEYWORD."/".$post->pictures[0]->src_original ?>" alt="<?= urlencode($post->title) ?>" width="1225" height="450" style="max-height: 450px;">
											</div>

											<?php } elseif ( count($post->videos) > 0 ) { if ( strpos($post->videos[0]->url,'v=') !== false ) { $videourl = "https://www.youtube.com/embed/".explode('v=',$post->videos[0]->url)[1]; } else { $videourl = $post->videos[0]->url; } ?>

											<div class="post-media animation flipInY">
												<div class="flex-video">
													<iframe width="1280" height="720" src="<?= $videourl ?>?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen style="border-radius: 4px;"></iframe>
												</div>
											</div>

											<?php } ?>

											<!--
											<div class="post-media">
												<div class="flex-video vimeo">
													<iframe src="http://player.vimeo.com/video/23237102?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="1280" height="720" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
												</div>
											</div>
											-->

											<div class="post-info">
												<h3 class="post-title"><a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>"><?= $post->title ?></a></h3>
											</div>  
											<div class="post-content" style="overflow: hidden;">
												<div style="max-height: 100px;"><?= nl2br(substr($post->text, 0, 150)) ?></div>

												<ul class="list-inline post-meta-info s24">
													<li>
														<a href="{url}posts/like/<?= $post->id ?>" class="ToolTip" title="Vind ik leuk">
															<span class="livicon" data-name="thumbs-up" data-size="24" data-color="#c0c0ca" data-hovercolor="#99B021"></span> 
															<?= $post->likes ?>
														</a>
													</li>
													<li>
														<a href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>/#post_comments" class="ToolTip" title="Reageren">
															<span class="livicon" data-name="comments" data-size="24" data-color="#c0c0ca" data-hovercolor="#99B021"></span> 
															<?= $post->comments ?>
														</a>
													</li>
													<li>
														<a href="" class="ToolTip" title="Delen op Facebook">
															<span class="livicon" data-name="redo" data-size="24" data-color="#c0c0ca" data-hovercolor="#99B021"></span> 
															<?= $post->shared ?>
														</a>
													</li>
												</ul>
												<a class="btn btn-primary btn-sm" href="{url}posts/view/<?= $post->id ?>/<?= urlencode($post->title) ?>" style="float: right;">Bekijken<i class="fa iconright fa-arrow-circle-right"></i></a>
											</div>

										</div> 

									</div>

	<?php } ?>
	
</div>
</div>
<!-- /Blog Content -->

<!-- Pagination -->
<div class="row">                        
<div class="col-sm-12">
	<?php if($paginate){ ?>
	<div class="text-center">
		<ul class="pagination">
			<?php if($page > 0){ ?>
			<li><a href="<?= $paging_url."?page=".($page - 1) ?>" aria-label="Vorige"><i class="fa fa-angle-left"></i></a></li>
			<?php } else { ?>
			<li class="disabled"><a href="#" onclick="return false;"><i class="fa fa-angle-left"></i></a></li>
			<?php } ?>
			<?php 
			for($i = 0; $i < ceil($all_posts / $settings->page_count); $i++)
			{  
				if($i == $page)
				{
					?>
					<li class="active"><a href="<?= $paging_url."?page=".$i ?>"><?= $i+1 ?></a></li>
					<?php
				}
				else
				{
					?>
					<li><a href="<?= $paging_url."?page=".$i ?>"><?= $i+1 ?></a></li>
					<?php
				}
			} 
			?>
			<?php if($page < ceil($all_posts / $settings->page_count)-1){ ?>
			<li><a href="<?= $paging_url."?page=".($page + 1) ?>" aria-label="Volgende"><i class="fa fa-angle-right"></i></a></li>
			<?php } else { ?>
			<li class="disabled"><a href="#" onclick="return false;"><i class="fa fa-angle-right"></i></a></li>
			<?php } ?>
		</ul>
	</div>
	<?php } ?>
	
</div>                                                  
</div>
<!-- /Pagination -->

<?php } ?>

<script type="text/javascript">
	function fbShare(url, winWidth, winHeight) {
        var winTop = (screen.height / 2) - (winHeight / 2);
        var winLeft = (screen.width / 2) - (winWidth / 2);
        window.open('http://www.facebook.com/sharer.php?s=100&p[url]=' + url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
    }
</script>