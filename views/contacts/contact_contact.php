	<?= $success_message != "" ? "<div class='success'>".$success_message."</div>" : ""; ?>
	<?= $error_message != "" ? "<div class='error'>".$error_message."</div>" : ""; ?>
	<form action="" class="form-horizontal"  role="form" name="form_contact" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<?php foreach($fields as $field){ ?>
		<div class="col-sm-12">
			<?php if($settings->labels > 0){ ?>
			<label for="<?= $this->contact->form_id($field->label) ?>"><?= $field->label ?><?= $field->required == 2 ? " *" : "" ?></label>
			<?php } ?>
			
				<?php 
				switch($field->type)
				{
					case "text":
						?>
						<input 
							type="text" 
							name="<?= $this->contact->form_name($field->label) ?>" 
							id="<?= $this->contact->form_id($field->label) ?>"
							<?= $field->class != "" ? ' class="'.$field->class.'"' : '' ?> 
							<?= $settings->placeholders > 0 ? ' placeholder="'.$field->label.'"' : '' ?> 
							value="<?= $field->value ?>" />
						<?php
						break;
					case "textarea":
						?>
						<textarea 
							name="<?= $this->contact->form_name($field->label) ?>" 
							 rows = "5" 
							id="<?= $this->contact->form_id($field->label) ?>"
							<?= $field->class != "" ? ' class="'.$field->class.'"' : '' ?> 
							<?= $settings->placeholders > 0 ? ' placeholder="'.$field->label.'"' : '' ?>><?= $field->value ?></textarea>
						<?php
						break;
					case "checkbox":
						?>
						<input 
							type="checkbox" 
							name="<?= $this->contact->form_name($field->label) ?>" 
							id="<?= $this->contact->form_id($field->label) ?>"
							<?= $field->class != "" ? ' class="'.$field->class.'"' : '' ?> 
							value="1"
							<?= $field->value == 1 ? ' checked="checked"' : '' ?> />
						<?php
						break;
					case "radio":
						if($settings->labels == 0){ 
							?>
							<label for="<?= $this->contact->form_id($field->label) ?>"><?= $field->label ?><?= $field->required == 2 ? " *" : "" ?></label>
							<?php
						}
						foreach(explode('##',$types[$field->type_id]->regex) as $field_val) {
						$info = explode('::',$field_val);
						?>
						<div class="radio_val">
							<input 
								type="radio"
								name="<?= $this->contact->form_name($field->label) ?>" 
								id="<?= $this->contact->form_name($field->label.$info[1]) ?>" 
								<?= $field->class != "" ? ' class="'.$field->class.'"' : '' ?> 
								value="<?= $info[1] ?>" 
								<?= $field->value == $info[1] ? ' checked="checked"' : '' ?> /> 
							<?= $info[0] ?>
						</div>
						<?php
						}
						break;
					case "file":
						?>
						<div class="upload_button" id="upload_<?= $this->contact->form_id($field->label) ?>"><?= $field->label ?> uploaden</div>
						<span class="upload_button_text" id="text_<?= $this->contact->form_id($field->label) ?>"></span>
						<div style='height: 0px;width: 0px; overflow:hidden;'>
							<input 
								type="file" 
								name="<?= $this->contact->form_name($field->label) ?>" 
								id="<?= $this->contact->form_id($field->label) ?>"
								<?= $field->class != "" ? ' class="'.$field->class.'"' : '' ?> />
						</div>
						<script type="text/javascript">
							$("#upload_<?= $this->contact->form_id($field->label) ?>").on('click', function(){
								$("#<?= $this->contact->form_id($field->label) ?>").click();
							});
							$("#<?= $this->contact->form_id($field->label) ?>").on('change', function(){
								var file = this.value;
								var fileName = file.split("\\");
								$("#text_<?= $this->contact->form_id($field->label) ?>").html(fileName[fileName.length-1]);
							});
						</script>
						<?php
						break;
					case "autocomplete":
						?>
						<div class="prod_complete">
							<div class="find">
								<input 
								type="text" 
								<?= $field->class != "" ? ' class="'.$field->class.'"' : '' ?> 
								<?= ' placeholder="Zoek product.."' ?> 
								value="" />
								<a class="add"></a>
							</div>
							<div class="added"></div>
							<input 
								name="<?= $this->contact->form_name($field->label) ?>" 
								id="<?= $this->contact->form_id($field->label) ?>" 
								value="" />
						</div>
						<script type="text/javascript">
							var prod_ads = new Array();
							$(function() {
								<?php foreach(explode('##', $field->value) as $val){ if($val != ""){?>
								prod_ads.push(new Array(1,'<?= $val ?>'));
								<?php } } ?>
								$( ".prod_complete .find input" ).autocomplete({
									source: "{url}contacts/ajax_products/",
									minLength: 2,
									select: function( event, ui ) {
										if(ui.item)
										{
											add_item(ui.item.value);
											return false;
										}
									}
								});
								$(".prod_complete .find a").on('click', function(){
									if($(".prod_complete .find input").val() != "")
									{
										add_item($(".prod_complete .find input").val());
									}
								});
								$(document).on('click',".prod_complete .added a",function(){
									for (i = 0; i < prod_ads.length; i++) {
										if(prod_ads[i][1] == $(this).parent().find('span').text())
										{
											prod_ads.splice(i, 1);
										}
									}
									build_list();
								});
							});
							function add_item(prod)
							{
								fnd = false;
								for (i = 0; i < prod_ads.length; i++) {
									if(prod_ads[i][1] == prod)
									{
										fnd = true;
										prod_ads[i][0]++;
									}
								}
								if(!fnd)
								{
									prod_ads.push(new Array(1,prod));
								}
								$(".prod_complete .find input").val('');
								build_list();
							}
							function build_list()
							{
								$input = $("#<?= $this->contact->form_id($field->label) ?>");
								$input.val('');
								$(".prod_complete .added").html('');
								for (i = 0; i < prod_ads.length; i++) {
									if(i > 0)
									{
										$input.val($input.val()+'##');
									}
									$input.val($input.val()+prod_ads[i][1]);
									
									$(".prod_complete .added").append('<div><span>'+prod_ads[i][1]+'</span><a></a></div>');
								}
							}
						</script>
						<?php
						break;
				}
				?>
			
		</div>
		<?php } ?>
		<div class="form_row actions">
			<input type="submit" class="btn btn-primary center-block" name="btn_submit" value="Verzend bericht" />
		</div>
		</div>
	</form>