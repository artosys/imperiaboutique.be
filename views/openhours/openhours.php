	<div class="pricing-table">
		<div class="pricing-table-content">
			<ul class="pricing-list">
				<?php
				
				$day = jddayofweek(gregoriantojd(date("m"),date("d"),date("Y")));
				$open = FALSE;
				
				for($i = 0; $i < 7; $i++)
				{
					if ( $day == $i )
					{
						if ( $day == 0 ) { $dag = 7; } else { $dag = $day; }
						
						if(isset($openhours[$dag]))
						{
							$start1 = strtotime($openhours[$dag]->start_1);
							$start2 = strtotime($openhours[$dag]->start_2);
							$end1   = strtotime($openhours[$dag]->end_1);
							$end2   = strtotime($openhours[$dag]->end_2);
							
							if($start2 == $end1)
							{
								if ( time() >= $start1 && time() < $end2 ) { $open = TRUE; }
							}
							else
							{
								if ( (time() >= $start1 && time() < $end1) || (time() >= $start2 && time() < $end2) ) { $open = TRUE; }
							}
						}
					}
				}
				
				if ( $open )
				{
					echo '<li style="color: #95a252; text-align: center; font-size: 1.5em;">Wij zijn momenteel open</li>';
				}
				else
				{
					echo '<li style="color: #B54444; text-align: center; font-size: 1.5em;">Wij zijn momenteel gesloten</li>';
				}
				
				?>
				<?php 
				$names = ($settings->short_names == 1) ? $this->config->item('cal_days_short') : $this->config->item('cal_days');
				for($i = 0; $i < 7; $i++)
				{
					echo '<li><span class="icon gfx-clock color-default iconleft"></span>'.$names[$i].':&nbsp; ';

					if(isset($openhours[$i+1]))
					{
						$start1 = strtotime($openhours[$i+1]->start_1);
						$start2 = strtotime($openhours[$i+1]->start_2);
						$end1   = strtotime($openhours[$i+1]->end_1);
						$end2   = strtotime($openhours[$i+1]->end_2);

						if($start2 == $end1)
						{
							echo date("H\ui",$start1).' - '.date("H\ui",$end2);
						}
						else
						{
							echo date("H\ui",$start1).' - '.date("H\ui",$end1);
							echo ' en '.date("H\ui",$start2).' - '.date("H\ui",$end2);
						}
					}
					else
					{
						echo 'Gesloten';
					}
					echo '</li>';
				}
				?>
			</ul>
		</div>
    </div>