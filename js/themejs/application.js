/* -------------------------------------------------------------------------------- /
	
	Avendor jQuery
	Created by 4grafx
	v1.0 - 20.02.2014
	All rights reserved.

	+----------------------------------------------------+
		TABLE OF CONTENTS
	+----------------------------------------------------+
	
	[1]		Page Preloader
	[2]		Initialize Boostrap
	[3]		Main Menu
	[4]		Smooth Scroll to Section
	[5]		Sticky Nav Bar
	[6]		Panel Slide
	[7]		Revolution Slider
	[8]		Revolution Shop Slider
	[9]		Portfolio
	[10]	OWL Carousel
	[11]	Parallax
	[12]	Lightbox
	[13]	Tooltips
	[14]	Animation on Scroll
	[15]	Google Maps
	[16]	Counters
	[17]	Pie Chart
	[18]	Twitter
	[19]	Back to top
	[20]	Under Construction Counter
	[21]	Animate Demo - remove on production websites
	
/ -------------------------------------------------------------------------------- */


/* ---------------------------------------------------
	Page Preloader
-------------------------------------------------- */



    $("body").queryLoader2({
        barColor: "#32313b",
        backgroundColor: "#1b1b22",
        percentage: true,
        barHeight: 1,
        completeAnimation: "grow",
        minimumTime: 100,
		onLoadComplete: hidePreLoader
		
    });

function hidePreLoader() {
        $("#preloader").hide();
    }


/* ---------------------------------------------------
	Initialize Boostrap
-------------------------------------------------- */

!function ($) {

  $(function(){
    // Bootstrap Tooltip
    $("[data-toggle=tooltip-boot]").tooltip()

    // Bootstrap Popover
    $("[data-toggle=popover]")
      .popover()
		}
	)
}(window.jQuery)

/* ---------------------------------------------------
	Main Menu
-------------------------------------------------- */


$('.navbar .dropdown').hover(function() {
$(this).addClass('open').find('.dropdown-menu').first().stop(true, true).slideDown(300);
    }, function() {
$(this).removeClass('open').find('.dropdown-menu').first().stop(true, true).hide(300);
    });

$('.navbar .dropdown > a').click(function(){
location.href = this.href;
});


$(document).on('click', '.gfx-nav .dropdown-menu', function(e) {
  e.stopPropagation()
})


/* ---------------------------------------------------
	Smooth Scroll to Section
-------------------------------------------------- */

    $(function() {
        $('a.smooth-scroll[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 2000, 'easeInOutExpo');
                    return false;
                }
            }
        });
    });


/* ---------------------------------------------------
	Sticky Bar
-------------------------------------------------- */

            $(function() {

                $(".header-main").stickOnScroll({
                    topOffset: 0,
                    setParentOnStick:   true
                });
				
				 var shrinkHeader = 600;
  					$(window).scroll(function() {
    				var scroll = getCurrentScroll();
      					if ( scroll >= shrinkHeader ) {
           					$('.header-main').addClass('shrink');
        					}
       					else {
            				$('.header-main').removeClass('shrink');
        					 }
						});
				function getCurrentScroll() {
				return window.pageYOffset || document.documentElement.scrollTop;
				}
                
            });


/* ---------------------------------------------------
	Panel Slide
-------------------------------------------------- */


$(document).ready(function(){

	$(".slide-panel-btn").click(function(){
		$("#slide-panel").slideToggle(250);
		$(this).toggleClass("active"); return false;
	});
	
	 
});



/* ---------------------------------------------------
	Revolution Slider
-------------------------------------------------- */
				jQuery(document).ready(function() {
				
					
								
					jQuery('.tp-banner').show().revolution(
					{
						dottedOverlay:"threexthree",
						delay:16000,
						startwidth:1170,
						startheight:700,
						hideThumbs:200,
						
						thumbWidth:100,
						thumbHeight:50,
						thumbAmount:3,
						
						navigationType:"none",
						navigationArrows:"solo",
						navigationStyle:"preview4",
						
						touchenabled:"on",
						onHoverStop:"on",
						
						swipe_velocity: 0.7,
						swipe_min_touches: 1,
						swipe_max_touches: 1,
						drag_block_vertical: false,
												
						parallax:"scroll",
						parallaxBgFreeze:"on",
						parallaxLevels:[10,20,30,40,50,60,70,80,90,100],
												
						keyboardNavigation:"off",
						
						navigationHAlign:"center",
						navigationVAlign:"bottom",
						navigationHOffset:0,
						navigationVOffset:20,

						soloArrowLeftHalign:"left",
						soloArrowLeftValign:"center",
						soloArrowLeftHOffset:20,
						soloArrowLeftVOffset:0,

						soloArrowRightHalign:"right",
						soloArrowRightValign:"center",
						soloArrowRightHOffset:20,
						soloArrowRightVOffset:0,
								
						shadow:0,
						fullWidth:"off",
						fullScreen:"on",

						spinner:"spinner4",
						
						stopLoop:"off",
						stopAfterLoops:-1,
						stopAtSlide:-1,

						shuffle:"off",
						
						autoHeight:"off",						
						forceFullWidth:"off",												
												
						hideThumbsOnMobile:"off",
						hideNavDelayOnMobile:1500,						
						hideBulletsOnMobile:"off",
						hideArrowsOnMobile:"off",
						hideThumbsUnderResolution:0,
						
						hideSliderAtLimit:0,
						hideCaptionAtLimit:0,
						hideAllCaptionAtLilmit:0,
						startWithSlide:0,
						fullScreenOffsetContainer: ".header-wrapper"	
					});
					
					
					
									
				});	//ready

/* ---------------------------------------------------
	Revolution Shop Slider
-------------------------------------------------- */
				jQuery(document).ready(function() {
				
					
								
					jQuery('.shop-slider').show().revolution(
					{
						dottedOverlay:"none",
						delay:16000,
						startwidth:1170,
						startheight:600,
						hideThumbs:200,
						
						thumbWidth:100,
						thumbHeight:50,
						thumbAmount:3,
						
						navigationType:"none",
						navigationArrows:"solo",
						navigationStyle:"preview4",
						
						touchenabled:"on",
						onHoverStop:"on",
						
						swipe_velocity: 0.7,
						swipe_min_touches: 1,
						swipe_max_touches: 1,
						drag_block_vertical: false,
												
						parallax:"scroll",
						parallaxBgFreeze:"on",
						parallaxLevels:[10,20,30,40,50,60,70,80,90,100],
												
						keyboardNavigation:"off",
						
						navigationHAlign:"center",
						navigationVAlign:"bottom",
						navigationHOffset:0,
						navigationVOffset:20,

						soloArrowLeftHalign:"left",
						soloArrowLeftValign:"center",
						soloArrowLeftHOffset:20,
						soloArrowLeftVOffset:0,

						soloArrowRightHalign:"right",
						soloArrowRightValign:"center",
						soloArrowRightHOffset:20,
						soloArrowRightVOffset:0,
								
						shadow:0,
						fullWidth:"off",
						fullScreen:"off",

						spinner:"spinner4",
						
						stopLoop:"off",
						stopAfterLoops:-1,
						stopAtSlide:-1,

						shuffle:"off",
						
						autoHeight:"off",						
						forceFullWidth:"off",												
												
						hideThumbsOnMobile:"off",
						hideNavDelayOnMobile:1500,						
						hideBulletsOnMobile:"off",
						hideArrowsOnMobile:"off",
						hideThumbsUnderResolution:0,
						
						hideSliderAtLimit:0,
						hideCaptionAtLimit:0,
						hideAllCaptionAtLilmit:0,
						startWithSlide:0,
						fullScreenOffsetContainer: ".header-wrapper"	
					});
					
					
					
									
				});	//ready

/* ---------------------------------------------------
	Portfolio
-------------------------------------------------- */

		$(document).ready(function(){
			
		
			
			// "colio" plugin
			$('.portfolio .portfolio-grid').colio({
				theme: '$',
				placement: 'after',
				scrollOffset: 60,
				expandLink: '.portfolio-expand',          // selector for element to expand colio viewport  
    			expandDuration: 900,                // duration of expand animation, ms  
    			expandEasing: 'swing',              // easing for expand animation  
    			collapseDuration: 500,              // duration of collapse animation, ms  
    			collapseEasing: 'swing',            // easing for collapse animation  
    			scrollDuration: 1300,                // page scroll duration, ms  
    			scrollEasing: 'swing',              // page scroll easing  
    			syncScroll: true,                  // sync page scroll with expand/collapse of colio viewport 
    			contentFadeIn: 900,                 // content fade-in duration, ms  
    			contentFadeOut: 300,                // content fade-out duration, ms  
    			contentDelay: 600,                  // content fade-in delay on expand, ms 
				closeText: '<span>X</span>',    // text/html for close button  
				nextText: '<span></span>',      // text/html for next button  
    			prevText: '<span></span>',      // text/html for previous button  
				hiddenItems: '.isotope-hidden',
				onExpand: 	function(content){$(".colio").resize() },

				onContent: function(content){
	
						$('.carousel-box-portfolio .carousel',content).each(function(index, element) {
							
							var carousel = $(this).closest('.carousel-box-portfolio');
							
							$(this).owlCarousel({
								//direction:'rtl', //use if your site is RTL
								autoPlay	 	 : $(this).data('carousel-autoplay'),
								items		 	 : $(this).data('carousel-items'),
								navigation		 : $(this).data('carousel-nav'),
								pagination		 : $(this).data('carousel-pagination'),
								singleItem		 : $(this).data('carousel-single'),
								transitionStyle	 : $(this).data('carousel-transition'),
								slideSpeed	     : $(this).data('carousel-speed'),
								navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
							}) 
							
						});

						
						if ($().magnificPopup) {
												  $('[data-lightbox=image], .lightbox').each(function(index, element) {$(this).magnificPopup({type:'image',	mainClass: 'mfp-fade',removalDelay: 300, fixedContentPos: false,  fixedBgPos: true,  overflowY: 'auto',	closeOnContentClick: true}); });
												  $('[data-lightbox=video], [data-lightbox=map], [data-lightbox=iframe], .lightbox-video, .lightbox-map, .lightbox-iframe').each(function(index, element) {$(this).magnificPopup({mainClass: 'mfp-fade', removalDelay: 300, fixedContentPos: false, fixedBgPos: true, overflowY: 'auto',  type: 'iframe',  fixedContentPos: false});});
												  $('[data-lightbox=gallery], .lightbox-gallery').each(function(index, element) {$(this).magnificPopup({mainClass: 'mfp-fade',
removalDelay: 300, fixedContentPos: false, fixedBgPos: true, overflowY: 'auto',  type: 'image', delegate: 'a', gallery: { enabled: true}});});
												};
					}

			});


			
			// "isotope" plugin
			var $container = $('.portfolio-grid');
			$container.imagesLoaded( function(){
				$container.isotope({
					
				});
			});	

			var filter = '*', isotope_run = function(f) {
				$container.isotope({filter: f}).
				trigger('colio','excludeHidden');
			};						
			$('#filters a').click(function(){
				var selector = $(this).attr('data-filter');
				$container.isotope({ filter: selector });
				return false;
			});
			
			// set selected menu items
			var $optionSets = $('.option-set'),
			$optionLinks = $optionSets.find('a'); 
			$optionLinks.click(function(){
				var $this = $(this);
				// don't proceed if already selected
				if ( $this.hasClass('selected') ) {
				return false;
				}
			var $optionSet = $this.parents('.option-set');
			$optionSet.find('.selected').removeClass('selected');
			$this.addClass('selected'); 
			});
			
			isotope_run(filter);
			
		});

				
/* ---------------------------------------------------
	OWL Carousel
-------------------------------------------------- */

  $('.carousel-box .carousel').each(function () {
	var carousel = $(this).closest('.carousel-box');

		$(this).owlCarousel({
			//direction		 :'rtl', //use if your site is RTL
	  		autoPlay	 	 : $(this).data('carousel-autoplay'),
	  		items		 	 : $(this).data('carousel-items'),
			navigation		 : $(this).data('carousel-nav'),
			pagination		 : $(this).data('carousel-pagination'),
			singleItem		 : $(this).data('carousel-single'),
			transitionStyle	 : $(this).data('carousel-transition'),
			slideSpeed	     : $(this).data('carousel-speed'),
			navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
			lazyLoad : true,
	  		autoHeight : true,
		})
  });


/* Sync Carousel */

    $(document).ready(function() {
     
    var sync1 = $("#full-sync");
    var sync2 = $("#thumb-sync");
     
    sync1.owlCarousel({
    singleItem : true,
    slideSpeed : 1000,
    navigation: false,
    pagination:false,
	transitionStyle : "fade",
    afterAction : syncPosition,
    responsiveRefreshRate : 200,
    });
     
    sync2.owlCarousel({
    items : 3,
	itemsDesktop : [1199,3],
	itemsDesktopSmall : [979,3],
	itemsTablet : [768,3],
	itemsMobile : [479,2],
    pagination:false,
    responsiveRefreshRate : 100,
    afterInit : function(el){
    el.find(".owl-item").eq(0).addClass("synced");
    }
    });
     
    function syncPosition(el){
    var current = this.currentItem;
    $("#thumb-sync")
    .find(".owl-item")
    .removeClass("synced")
    .eq(current)
    .addClass("synced")
    if($("#thumb-sync").data("owlCarousel") !== undefined){
    center(current)
    }
    }
     
    $("#thumb-sync").on("click", ".owl-item", function(e){
    e.preventDefault();
    var number = $(this).data("owlItem");
    sync1.trigger("owl.goTo",number);
    });
     
    function center(number){
    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
    var num = number;
    var found = false;
    for(var i in sync2visible){
    if(num === sync2visible[i]){
    var found = true;
    }
    }
     
    if(found===false){
    if(num>sync2visible[sync2visible.length-1]){
    sync2.trigger("owl.goTo", num - sync2visible.length+2)
    }else{
    if(num - 1 === -1){
    num = 0;
    }
    sync2.trigger("owl.goTo", num);
    }
    } else if(num === sync2visible[sync2visible.length-1]){
    sync2.trigger("owl.goTo", sync2visible[1])
    } else if(num === sync2visible[0]){
    sync2.trigger("owl.goTo", num-1)
    }
    }
     
    });




/* ---------------------------------------------------
	Parallax
-------------------------------------------------- */


		$(function(){
			$.stellar({
				horizontalScrolling: false,
				verticalOffset: 0,
			});
		});


/* ---------------------------------------------------
	Lightbox
-------------------------------------------------- */


if ($().magnificPopup) {
	$('[data-lightbox=image], .lightbox').each(function(index, element) {
		$(this).magnificPopup({
			type:'image',
			mainClass: 'mfp-fade',
			fixedContentPos: false,
           	fixedBgPos: true,
           	overflowY: 'auto',
			removalDelay: 300,
			closeOnContentClick: true,
			
		});		
	});
	
	$('[data-lightbox=video], [data-lightbox=map], [data-lightbox=iframe], .lightbox-video, .lightbox-map, .lightbox-iframe').each(function(index, element) {
		$(this).magnificPopup({
			mainClass: 'mfp-fade',
			removalDelay: 300,
			fixedContentPos: false,
           	fixedBgPos: true,
           	overflowY: 'auto',
		  	type: 'iframe',
		  	fixedContentPos: false
		});
	});
	
	$('[data-lightbox=gallery], .lightbox-gallery').each(function(index, element) {
		$(this).magnificPopup({
			mainClass: 'mfp-fade',
			removalDelay: 300,
			fixedContentPos: false,
           	fixedBgPos: true,
           	overflowY: 'auto',
		  	type: 'image',
		  	delegate: 'a',
			gallery: {
				enabled: true
			}
		});
	});
	
	
};


/* ---------------------------------------------------
	Tooltips
-------------------------------------------------- */

        $(document).ready(function() {
            $(".ToolTip").OpieTooltip({     

            });
        }); 

/* ---------------------------------------------------
	Animation on Scroll
-------------------------------------------------- */	
		
$('.animation').waypoint(function(direction) {
  $(this).addClass('animation-active');
}, { 	offset: '100%',
triggerOnce: true });

/* ---------------------------------------------------
	Google Maps
-------------------------------------------------- */
$(document).ready(function () {

if ($('#google-map-footer').length>0) {
	var e=new google.maps.LatLng(51.1575261, 4.9871797),
		o={zoom:17,center:new google.maps.LatLng(51.1575261, 4.9871797),
		mapTypeId:google.maps.MapTypeId.ROADMAP,
		mapTypeControl:false,
		scrollwheel:false,
		zoomControl: true,
		zoomControlOptions: {style: google.maps.ZoomControlStyle.SMALL},
		draggable:!0,
		navigationControl:!1,
		styles: [	{featureType:"administrative",elementType:"all",stylers:[{visibility:"on"},{saturation:-100},{lightness:20}]},	{featureType:"road",elementType:"all",stylers:[{visibility:"on"},{saturation:-100},{lightness:40}]},	{featureType:"water",elementType:"all",stylers:[{visibility:"on"},{saturation:-10},{lightness:30}]},	{featureType:"landscape.man_made",elementType:"all",stylers:[{visibility:"simplified"},{saturation:-60},{lightness:10}]},	{featureType:"landscape.natural",elementType:"all",stylers:[{visibility:"simplified"},{saturation:-60},{lightness:60}]},	{featureType:"poi",elementType:"all",stylers:[{visibility:"off"},{saturation:-100},{lightness:60}]},	{featureType:"transit",elementType:"all",stylers:[{visibility:"off"},{saturation:-100},{lightness:60}]}]	
	},
		n=new google.maps.Map(document.getElementById("google-map-footer"),o);
		google.maps.event.addDomListener(window,"resize",function(){var e=n.getCenter();
		google.maps.event.trigger(n,"resize"),n.setCenter(e)});
		
		var g='<div class="map-marker"><h4 class="color-dark xbold">IMPERIA</h4><p>Pas 153</p><p>GEEL B-2440</p></div>',a=new google.maps.InfoWindow({content:g})
		,t=new google.maps.MarkerImage("img/theme/google-marker.png",new google.maps.Size(157,85),
		new google.maps.Point(0,0),new google.maps.Point(75,50)),
		i=new google.maps.LatLng(51.1575261, 4.9871797),
		p=new google.maps.Marker({position:i,map:n,icon:t,zIndex:3});
		google.maps.event.addListener(p,"click",function(){a.open(n,p)}),
		$(".gmap-button").click(function(){$("#google-map-footer").slideToggle(300,function(){google.maps.event.trigger(n,"resize"),n.setCenter(e)}),
		$(this).toggleClass("show-map")});
		
}

if ($('#google-map').length>0) {

	var e=new google.maps.LatLng(51.1575261, 4.9871797),
		o={zoom:17,center:new google.maps.LatLng(51.1575261, 4.9871797),
		mapTypeId:google.maps.MapTypeId.ROADMAP,
		mapTypeControl:false,
		scrollwheel:false,
		zoomControl: true,
		zoomControlOptions: {style: google.maps.ZoomControlStyle.SMALL},
		draggable:!0,
		navigationControl:!1,
		styles: [	{featureType:"administrative",elementType:"all",stylers:[{visibility:"on"},{saturation:-100},{lightness:20}]},	{featureType:"road",elementType:"all",stylers:[{visibility:"on"},{saturation:-100},{lightness:40}]},	{featureType:"water",elementType:"all",stylers:[{visibility:"on"},{saturation:-10},{lightness:30}]},	{featureType:"landscape.man_made",elementType:"all",stylers:[{visibility:"simplified"},{saturation:-60},{lightness:10}]},	{featureType:"landscape.natural",elementType:"all",stylers:[{visibility:"simplified"},{saturation:-60},{lightness:60}]},	{featureType:"poi",elementType:"all",stylers:[{visibility:"off"},{saturation:-100},{lightness:60}]},	{featureType:"transit",elementType:"all",stylers:[{visibility:"off"},{saturation:-100},{lightness:60}]}]	
	},
		n=new google.maps.Map(document.getElementById("google-map"),o);
		google.maps.event.addDomListener(window,"resize",function(){var e=n.getCenter();
		google.maps.event.trigger(n,"resize"),n.setCenter(e)});
		
		var g='<div class="map-marker"><h4 class="color-dark xbold">IMPERIA</h4><p>Pas 153</p><p>GEEL B-2440</p></div>',a=new google.maps.InfoWindow({content:g})
		,t=new google.maps.MarkerImage("img/theme/google-marker.png",new google.maps.Size(157,85),
		new google.maps.Point(0,0),new google.maps.Point(75,50)),
		i=new google.maps.LatLng(51.1575261, 4.9871797),
		p=new google.maps.Marker({position:i,map:n,icon:t,zIndex:3});
		google.maps.event.addListener(p,"click",function(){a.open(n,p)}),
		$(".gmap-button").click(function(){$("#google-map").slideToggle(300,function(){google.maps.event.trigger(n,"resize"),n.setCenter(e)}),
		$(this).toggleClass("show-map")});

}

});

/* ---------------------------------------------------
	Team Members
-------------------------------------------------- */
$(".team-wrapper").on("click", function() {
    $(this).toggleClass('cardtoggle').siblings().removeClass('cardtoggle');
});

/* ---------------------------------------------------
	Counters
-------------------------------------------------- */

$('.counter').waypoint(function(direction) {
  $(this).addClass('timer');
  $('.timer').countTo()
}, { 	offset: '100%',
triggerOnce: true });

$('.timer').countTo()


/* ---------------------------------------------------
	Pie Chart
-------------------------------------------------- */

$(document).ready(function() 
	
	{ jQuery(".chart").waypoint(function(direction) { 
	
  $('.progress-pie .chart').each(function () {
	var carousel = $(this).closest('.progress-pie');

    $(this).easyPieChart({
		barColor: $(this).data('bar-color'),
		trackColor: $(this).data('track-color'),
		scaleColor: $(this).data('scale-color'),
		lineWidth: $(this).data('line-width'),
		lineCap: "butt",
		scaleLength: 6,
		size:180,
		rotate: 0,
		animate:2000,
        
    })
  });	
	}, { offset: '100%',
triggerOnce: true }); });



/* ---------------------------------------------------
	Video BG
-------------------------------------------------- */



/* Shop Input */
$('.qup').on('click',function(){
    $('.input-quantity').val(parseInt($('.input-quantity').val())+1);
});

$('.qdown').on('click',function(){
    $('.input-quantity').val(parseInt($('.input-quantity').val())-1);
}); 


/* ---------------------------------------------------
	Back to Top
-------------------------------------------------- */

$(document).ready(function(){
 
	$(window).scroll(function(){
	    if ($(this).scrollTop() > 100) {
	        $('.scrollup').fadeIn();
	    } else {
	        $('.scrollup').fadeOut();
	    }
	});	

});

/* ---------------------------------------------------
	UnderConstruction Counter
-------------------------------------------------- */

      $(function() {
        var endDate = "May 12, 2015 09:03:25";

        $('.countdown.styled').countdown({
          date: endDate,
          render: function(data) {
            $(this.el).html("<div class='col-md-3 col-sm-6'><div class='counter-block'>" + this.leadingZeros(data.days, 3) + " <span>days</span></div></div><div class='col-md-3 col-sm-6'><div class='counter-block'>" + this.leadingZeros(data.hours, 2) + " <span>hrs</span></div></div><div class='col-md-3 col-sm-6'><div class='counter-block'>" + this.leadingZeros(data.min, 2) + " <span>min</span></div></div><div class='col-md-3 col-sm-6'><div class='counter-block'>" + this.leadingZeros(data.sec, 2) + " <span>sec</span></div></div>");
          }
        });


      });


/* ---------------------------------------------------
	Animate Demo - can be removed on production website
-------------------------------------------------- */
  function testAnim(x) {
    $('#animationSandbox').removeClass().addClass(x + ' animation animation-active').one('webkitAnimationEnd mozAnimationEnd oAnimationEnd animationEnd', function(){
      $(this).removeClass();
    });
  };

  $(document).ready(function(){
    $('.js--triggerAnimation').click(function(){
      var anim = $('.js--animations').val();
      testAnim(anim);
    });
  });



