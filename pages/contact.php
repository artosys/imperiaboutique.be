<!-- Main Wrapper Header -->
        <div class="main-wrapper-header fancy-header dark-header parallax parallax-breadcrumbs" data-stellar-background-ratio="0.4">
			
            <div class="bg-overlay bg-overlay-gdark"></div>

 			<div class="container">
            
				<div class="row">
                	<div class="col-sm-12 columns">
                		<div class="page-title">
                    		<h2>Contacteer ons</h2>                    
                    	</div>
                    	<div class="breadcrumbs-wrapper">               
							<ol class="breadcrumb">
  								<li><a href="{url}">Home</a></li>
								<li class="active">Contact</li>
							</ol>
                		</div>
					</div>
				</div>
                
			</div>
 
        </div>       
		<!-- /Main Wrapper Header -->

		<!-- Main Container -->
		<div class="main-wrapper">

        	<!-- Container -->
            <div class="container">
            	<div class="white-space space-big"></div>
                
            	<div class="row">
                    <div class="col-md-6">
                    	<h3 class="fancy-title"><span>Contact Formulier</span></h3>
						
                        <?= Modules::run('contacts/contacts_logic/show_form', 'partial', 'contact'); ?>
                                               
                        <div class="white-space space-small"></div>
                    </div>
                    
                    <div class="col-md-2"></div>
                    
                    <div class="col-md-4">
                    	<h3 class="fancy-title"><span>Contactgegevens</span></h3>
                        <div class="row">	
                        	                   <div class="pricing-table">
                                                	<div class="pricing-table-content">
                                                    	<ul class="pricing-list">
                                                        	<li><span class="icon gfx-phone color-default iconleft"></span><:imp_contact_tel:></li>
                                                            <li><span class="icon gfx-mail color-default iconleft"></span><:imp_contact_mail:></li>
                                                            <li><span class="icon gfx-home color-default iconleft"></span><:imp_contact_adres:></li>
                                                        </ul>
                                                    </div>
                                                </div>
                        </div>
                        <div class="white-space space-small"></div>
                        <h3 class="fancy-title"><span>Openingsuren</span></h3>
                        <div class="row">	
							<?= Modules::run('openhours/openhours_logic/show_hours', 'partial'); ?>
                        </div>
                        <div class="white-space space-small"></div>
                    </div>    
                              
			
            	<div class="white-space space-medium"></div>	
            </div>
            </div>
			<!-- /Container -->
          
        	<!-- Map -->
            <div class="gmap-wrapper"><div id="google-map"></div></div>
            <!-- /Map -->
                                   
		</div>
		<!-- /Main Container -->