<!-- Slider Container -->
		<div class="slider-wrapper">

			<div class="tp-banner-container">
				<div class="tp-banner" >
					<ul>

						<!-- SLIDE  -->
						<li data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="1000" data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7" data-saveperformance="off"  data-title="We Are Avendor">
							<!-- MAIN IMAGE -->
							<img src="img/demo/slider/revolution-slider1.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

							<div class="tp-caption large_bold_white customin customout tp-resizeme rs-parallaxlevel-3"
								data-x="center"
								data-y="150"

								data-splitin="words"
								data-elementdelay="0.1"
								data-start="1250"
								data-speed="1500"
								data-easing="Power3.easeOut"
								data-customin="x:0;y:-20;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"

								data-splitout="lines"
								data-endelementdelay="0"
								data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:10;scaleY:10;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
								data-endspeed="1500"
								data-endeasing="Power3.easeInOut"
								data-captionhidden="on"
								style="font-size:90px;line-height:110px;"><:imp_landing_hoofd:>
							</div>

							<div class="tp-caption mediumlarge_light_white script-font customin customout tp-resizeme rs-parallaxlevel-4"
								data-x="center"
								data-y="280"

								data-splitin="words"
								data-elementdelay="0.05"
								data-start="1650"
								data-speed="900"
								data-easing="Back.easeOut"
								data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
								data-endelementdelay="0.1"
								data-customout="x:-230;y:-20;z:0;rotationX:0;rotationY:0;rotationZ:90;scaleX:0.2;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%"
								data-endspeed="500"
								data-endeasing="Back.easeIn"
								data-captionhidden="on"
								style="z-index: 4; font-size:52px;"><:imp_landing_sub:>
							</div>

							<div class="tp-caption sfb rs-parallaxlevel-5"
								data-x="center"
								data-y="380"
								data-speed="800"
								data-start="2600"
								data-easing="Power4.easeOut"
								data-endspeed="300"
								data-endeasing="Power1.easeIn"
								data-captionhidden="off"
								style="z-index: 6">
								<a href="#welkom" class="btn btn-primary btn-lg">Welkom<i class="fa fa-angle-double-down iconright"></i></a>
							</div>

						</li>




						<div class="tp-bannertimer tp-bottom"></div>

					</ul>
				</div>
			</div>
    
		</div>
		<!-- /Slider Container -->
		
		<!-- Main Container -->
		<div class="main-wrapper" id="welkom">

        	<!-- Algemeen -->
            <div class="container">
            	<div class="white-space space-big"></div>

            	<div class="row">
                    <div class="col-sm-8 columns">
                    	<h4 class="fancy-title animation fadeInRight"><span><:imp_home_title_collapse:></span></h4>
							<!-- Informatie -->
							<div class="accordion panel-group" id="accordion">

								<div class="panel panel-default">
									<div class="panel-heading">
										<h5 class="panel-title"><a class="accordion-toggle collapsed-icon" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="icon livicon iconleft" data-name="angle-double-right" data-size="16" data-color="#95a252" data-hovercolor="#99B021"></span><:imp_home_title_about_first:></a></h5>
									</div>
									<div id="collapseOne" class="panel-collapse collapse in">
										<div class="panel-body">
											<p><:imp_home_text_about_first:></p>
										</div>
									</div>
								</div>
  
								<div class="panel panel-default">
									<div class="panel-heading">
													<h5 class="panel-title"><a class="accordion-toggle collapsed-icon collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="icon livicon iconleft" data-name="angle-double-right" data-size="16" data-color="#95a252" data-hovercolor="#99B021"></span><:imp_home_title_about_second:></a></h5>
									</div>
									<div id="collapseTwo" class="panel-collapse collapse">
										<div class="panel-body">
											<p><:imp_home_text_about_second:></p> 
										</div>
									</div>
								</div>

								<div class="panel panel-default">
									<div class="panel-heading">
													<h5 class="panel-title"><a class="accordion-toggle collapsed-icon collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="icon livicon iconleft" data-name="angle-double-right" data-size="16" data-color="#95a252" data-hovercolor="#99B021"></span><:imp_home_title_about_third:></a></h5>
									</div>
									<div id="collapseThree" class="panel-collapse collapse">
										<div class="panel-body">
											<p><:imp_home_text_about_third:></p>
										</div>
									</div>
  								</div>

							</div>
							<!-- /Informatie -->                       
                    	<div class="white-space space-big"></div>
                    </div>
                    <div class="col-sm-4 columns">
                    	<h4 class="fancy-title animation fadeInLeft"><span>Openingsuren</span></h4>

                                <div class="row">	
                                                        
									<?= Modules::run('openhours/openhours_logic/show_hours', 'partial'); ?>
									
                                </div>                                        

                        <div class="white-space space-big"></div>
                    </div>
				</div>
	
            </div>
			<!-- /Algemeen -->

        	<!-- Laatste nieuws -->        
			<div class="parallax parallax-background10" data-stellar-background-ratio="0.4">            
            
            	<div class="bg-overlay bg-overlay-dark"></div>
                
				<div class="white-space space-big"></div>
                    
            		<div class="container">
            			<div class="row">
                			<div class="col-md-10 col-md-offset-1 columns">
                            
                            	<h2 class="fancy-title text-center color-white"><span>Laatste nieuws</span></h2>
								<div class="row">
                                	<div class="col-md-12">
            							<!-- Carousel -->
            							<div class="carousel-box" >
            								<div class="carousel carousel-simple" data-carousel-autoplay="9000" data-carousel-items="3" data-carousel-nav="false" data-carousel-pagination="true"  data-carousel-speed="1000">
												
												<?= Modules::run('posts/posts_logic/index', 'partial', '6'); ?>
                                                                                    
            								</div>
                                    	</div>
                                        <!-- /Carousel -->
                                    </div>
                                </div>
                                
                            </div>
                        </div>
					</div>                    
                    
                <div class="white-space space-big"></div>               
                
            </div>
			<!-- /Laatste nieuws -->
            
        	<!-- Onze merken -->
            <div class="container">
            	<div class="white-space space-big"></div>
                <h2 class="fancy-title text-center color-white"><span>Onze merken</span></h2>
            	<div class="row">
  					<div class="col-md-12 columns">
                    	<!-- Carousel -->
						<div class="carousel-box" >
							<div class="carousel carousel-simple" data-carousel-autoplay="6000" data-carousel-items="5" data-carousel-nav="false" data-carousel-pagination="false"  data-carousel-speed="1000">
                            
								<?= Modules::run('brands/brands_logic/index', 'partial', '10'); ?>
								
							</div>
                        </div>
                        <!-- /Carousel -->                    	
                    </div>
				</div>			
	
           		<div class="white-space space-big"></div>
            </div>
			<!-- /Onze merken -->
            
        	<!-- Volg ons op facebook -->        
			<div class="parallax parallax-background7" data-stellar-background-ratio="0.4">            
            
            	<div class="bg-overlay bg-overlay-dark"></div>
                
				<div class="white-space space-big"></div>

                <div class="container">
                        
                    <div class="col-md-10 col-md-offset-1 columns">  
                                          
                    	<div class="twitter-carousel color-white">
                          	<div class="iconbox-wrapper center-block circle border iconbox-2x animation bounceIn">
                        		<i class="fa fa-facebook"></i>
                        	</div>
                                    
                        	<div class="white-space space-small"></div>
							<a href="https://www.facebook.com/pages/Imperia-Geel/1616041218610680" target="_blank">
                            <button type="button" class="btn btn-success btn-alt margin-bottom10"><span class="livicon iconleft" data-n="thumbs-up" data-s="24" data-color="#ffffff" data-hovercolor="#99B021" data-onparent="true" id="like-on-facebook"></span>Like ons op Facebook!</button>
							</a>
						</div>
                                
                	</div>
                    
                </div>
                
                <div class="white-space space-big"></div>
                    
            </div>
            <!-- /Volg ons op facebook -->                    
                    
                         
		</div>
		<!-- /Main Container -->

