		<!-- Main Wrapper Header -->
        <div class="main-wrapper-header fancy-header dark-header parallax parallax-portfolio" data-stellar-background-ratio="0.4">
			
            <div class="bg-overlay bg-overlay-dark"></div>

 			<div class="container">
            
				<div class="row">
                	<div class="col-sm-12 columns">
                		<div class="page-title">
                    		<h2>Merken</h2>                    
                    	</div>
                    	<div class="breadcrumbs-wrapper">               
							<ol class="breadcrumb">
  								<li><a href="{url}">Home</a></li>
                                <li><a href="#">Merken</a></li>
							</ol>
                		</div>
					</div>
				</div>
                
			</div>
 
        </div>       
		<!-- /Main Wrapper Header -->

		<!-- Main Container -->
		<div class="main-wrapper">
            
			<div class="container">
            
				<div class="white-space space-big"></div>

				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h3 class="fancy-title text-center"><span>Onze merken</span></h3>
                	</div>
				</div>

				<div class="row">
                	<div class="col-md-12">
										
                    <!-- PORTFOLIO -->
					<div class="portfolio clearfix">                    
                        
						<!-- Portfolio Grid -->
						<div class="portfolio-grid portfolio-3-cols portfolio-classic">

							<?= Modules::run('brands/brands_logic/index', 'partial'); ?>

                        </div>
                        <!-- /Portfolio Grid -->
                        					
                    </div>
					<!-- /PORTFOLIO-->
                    </div>
				</div>
				
				<!--
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
                       	<div class="white-space space-medium"></div>
						<div class="text-center"><a class="btn btn-primary btn-lg">Bekijk alle merken</a></div>
                    </div>
				</div>
				-->
				
				<div class="white-space space-medium"></div>
                
			</div>                                               

		</div>
		<!-- /Main Container -->      