<!-- Main Wrapper Header -->
        <div class="main-wrapper-header fancy-header dark-header parallax parallax-blog" data-stellar-background-ratio="0.4">
			
            <div class="bg-overlay bg-overlay-dark"></div>

 			<div class="container">
            
				<div class="row">
                	<div class="col-sm-12 columns">
                		<div class="page-title">
                    		<h2>Nieuws</h2>                    
                    	</div>
                    	<div class="breadcrumbs-wrapper">               
							<ol class="breadcrumb">
  								<li><a href="#">Home</a></li>
								<li class="active">Nieuws</li>
							</ol>
                		</div>
					</div>
				</div>
                
			</div>
 
        </div>       
		<!-- /Main Wrapper Header --> 

<!-- Main Container -->
		<div class="main-wrapper">

        	<!-- Container -->
            <div class="container content-with-sidebar">
            
            	<div class="row">

					<div class="col-sm-12">                    
                    	<div class="white-space space-big"></div>
                        
						<?= Modules::run('posts/posts_logic/index', 'partial'); ?>
						
                        <div class="white-space space-big"></div>
                    </div>
                
                </div>          
            	
            </div>
			<!-- /Container -->
                                   
		</div>
		<!-- /Main Container -->