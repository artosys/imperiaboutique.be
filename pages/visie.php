<!-- Main Wrapper Header -->
        <div class="main-wrapper-header fancy-header dark-header parallax parallax-background11" data-stellar-background-ratio="0.4">

 			<div class="container">
            
				<div class="row">
                	<div class="col-sm-12 columns">
                		<div class="page-title">
                    		<h2>Visie</h2>                    
                    	</div>
                    	<div class="breadcrumbs-wrapper">               
							<ol class="breadcrumb">
  								<li><a href="{url}">Home</a></li>
								<li class="active">Visie</li>
							</ol>
                		</div>
					</div>
				</div>
                
			</div>
 
        </div>       
		<!-- /Main Wrapper Header -->

		<!-- Main Container -->
		<div class="main-wrapper">

        	<!-- Container -->
            <div class="container">
            	<div class="white-space space-big"></div>
                
            	<div class="row">
                    <div class="col-md-8 col-md-offset-2">
                    	<h3 class="fancy-title text-center animation fadeInDown"><span><:imp_about_title:></span></h3>
                        <div class="lead text-center"><em><:imp_about_slogan:></em></div>
                    	<div class="white-space space-medium"></div>
                    </div>					
				</div>
                
            	<div class="row">
                	<div class="col-sm-6">
                    	<h4 class="fancy-title animation fadeInLeft"><span><:imp_about_section_title_one:></span></h4>
						<p class="animation fadeInLeft"><:imp_about_section_text_one:></p>                        
                        
                        <div class="white-space space-small"></div>
                    </div>
                    <div class="col-sm-6 img-container" >
						<:imp_about_img_one:>
						<div class="white-space space-small"></div>
                    </div>
				</div>

            	<div class="row">
                    <div class="col-md-8 col-md-offset-2">
						<hr class="hr-fancy text-center"/>
                    </div>					
				</div>

            	<div class="row">
                    <div class="col-sm-6">
                    	<div class="white-space space-small"></div>
						<:imp_about_img_two:>
						<div class="white-space space-small"></div>
                    </div>
                	<div class="col-sm-6">
                    	<div class="white-space space-small"></div>
                    	<h4 class="fancy-title animation fadeInRight"><span><:imp_about_section_title_two:></span></h4>
						<p class="animation fadeInRight"><:imp_about_section_text_two:></p>                        
                        <!--<a href="#" class="btn btn-primary btn-sm btn-alt animation fadeInRight">Meer informatie</a>-->
                        <div class="white-space space-small"></div>
                    </div>
				</div>

            	<div class="row">
                    <div class="col-md-8 col-md-offset-2">
						<hr class="hr-fancy text-center"/>
                    </div>					
				</div>

            	<div class="row">
                	<div class="col-sm-6">
                    	<div class="white-space space-small"></div>
                    	<h4 class="fancy-title animation fadeInLeft"><span><:imp_about_section_title_three:></span></h4>
						<p class="animation fadeInLeft"><:imp_about_section_text_three:></p>                        
                        
                        <div class="white-space space-small"></div>
                    </div>
                    <div class="col-sm-6">
                    	<div class="white-space space-small"></div>
						<:imp_about_img_three:>
						<div class="white-space space-small"></div>
                    </div>
				</div>
                
                <div class="white-space space-medium"></div>
                
                <div class="row" style="padding-bottom:5%;">
                
                    <h3 class="fancy-title text-center animation fadeInDown"><span><:imp_about_team_header:></span></h3>
                
						<div class="col-sm-4" style="text-align:center;">
							<:imp_about_img_four:>
						</div>
						<div class="col-sm-8">
							<h4 class="fancy-title animation fadeInLeft"><span><:imp_about_team_name:></span></h4>
							<p class="animation fadeInLeft"><:imp_about_team_text:></p>                        
						</div>
						<div class="white-space space-big"></div>
                </div>
			</div>
	
            </div>
			<!-- /Container -->


        	<!-- Getuigenissen -->        
			<div class="parallax parallax-background8" data-stellar-background-ratio="0.4">            
            
            	<div class="bg-overlay bg-overlay-dark"></div>
                
				<div class="white-space space-big"></div>
                    
            		<div class="container">
            			<div class="row">
                			<div class="col-md-10 col-md-offset-1 columns">
                            
                            	<h3 class="fancy-title text-center color-white animation fadeInUp"><span><:imp_about_testo_title:></span></h3>
								<!-- Testimonials Carousel -->
								<div class="testimonials carousel-box animation fadeInUp">
                        			<div class="carousel" data-carousel-autoplay="6000" data-carousel-nav="false" data-carousel-pagination="true" data-carousel-single="true" data-carousel-transition="backSlide">
										<?= Modules::run('testimonials/testimonials_logic/index', 'partial'); ?>
                            		</div>
								</div>
                        		<!-- /Testimonials Carousel --> 
                                
                            </div>
                        </div>
					</div>                    
                    
                <div class="white-space space-big"></div>               
                
            </div>
			<!-- /Getuigenissen -->
                                   
		</div>
		<!-- /Main Container -->      