<?php
// -----------------------------------------------------------------------------
//
// Artosys Client Website
//
// -----------------------------------------------------------------------------

/*
 * -----------------------------------------------------------------------------
 *  Define the environment variables
 * -----------------------------------------------------------------------------
 */
	define('ENVIRONMENT', 'online');
	define('WEBSITE_KEYWORD', 'imperia');
	define('DEV_STATUS', 'live'); // Development|testing|live
	
/*
 * -----------------------------------------------------------------------------
 *  Define connection with artosystem
 * -----------------------------------------------------------------------------
 */
	if(ENVIRONMENT == "offline")
	{
		error_reporting(E_ALL);
		include "path.php";
	}
	else
	{
		error_reporting(0);
		define('SHAREPATH', "../../artocdn.be/htdocs/php/");
	}

/*
 * -----------------------------------------------------------------------------
 *  Connect to artosystem
 * -----------------------------------------------------------------------------
 */ 
	include (SHAREPATH."artosystem.php");