<?php 
	$title = "";
	if ($this->config->item('sys_active_page') == 'pages/home') { $title = "Home - "; }
	elseif ($this->config->item('sys_active_page') == 'pages/nieuws') { $title = "Nieuws - "; }
	elseif ($this->config->item('sys_active_page') == 'pages/visie') { $title = "Visie - "; }
	elseif ($this->config->item('sys_active_page') == 'pages/merken') { $title = "Merken - "; }
	elseif ($this->config->item('sys_active_page') == 'pages/contact') { $title = "Contact - "; }
	
	$description = "Imperia boutique biedt u stijlvolle Italiaanse damesmode en persoonlijk advies in het centrum van Geel.";
	if($this->config->item('article_description'))
	{
		$description = $this->config->item('article_description');
}
?>

<!DOCTYPE html>
<html lang="nl" >
	<head>
		<meta charset="utf-8">	
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="description" content="<?= $description ?>" />
		<meta name="keywords" content="Imperia, Italian, Boutique, Kleding, Geel, Stijlvol, Italiaans, Merken, Mode, Designers, Pas, Persoonlijk, Service, Advies" />
		<meta name="author" content="Imperia Boutique">
		<meta name="robots" content="index, follow" />
		<meta name="revisit-after" content="3 days" />
		<title><?= $title ?>Imperia - Italian boutique</title>
		
  		<link type="text/css" rel="stylesheet" href="<?= SHAREURL ?>css/bootstrap.3.3.1.css" />        
		<!-- <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.2.0-rc2/css/bootstrap-rtl.css"> -->
		<link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
		<link href="{url}css/avendor-font-styles.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="{url}css/revolutionslider/settings.css" media="screen" />     
    	<link href="{url}css/themecss/animate.css" rel="stylesheet">      
    	<link href="{url}css/themecss/lightbox.css" rel="stylesheet">     
    	<link href="{url}css/owl-carousel/owl.carousel.css" rel="stylesheet">
        <link href="{url}css/owl-carousel/owl.transitions.css" rel="stylesheet">
  		<link href="{url}css/avendor-dark.css" rel="stylesheet">
		<link href="{url}css/colors/color-default.css" rel="stylesheet" title="style1">
	    <link href='http://fonts.googleapis.com/css?family=Raleway:400,200,300,100,500,600,700,800,900' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
    	<link rel="shortcut icon" href="{url}ico/favicon.png">
		<link type="text/css" rel="stylesheet" href="{url}css/style.css" />
		<link type="text/css" rel="stylesheet" href="http://www.artocdn.be/css/jquery-ui.css" />
		<link type="text/css" rel="stylesheet" href="<?= SHAREURL ?>js/ms/style/masterslider.css" />
		
		<script type="text/javascript" src="<?= SHAREURL ?>js/jquery.1.11.1.js"></script>
		<script type="text/javascript" src="<?= SHAREURL ?>js/bootstrap.3.3.1.js"></script>
		
		<?php $this->layout->partial("platforms/pharma/views/manager_head", TRUE, $content_data); ?>
		<?php $this->layout->partial("platforms/black/views/manager_head", TRUE, $content_data); ?>
		
	</head>
	<body class="bg-pattern1" data-spy="scroll" data-target=".navbar">
	<div id="preloader"></div>
	
		<!-- BEHEERBALK -->
		<?php $this->layout->partial("platforms/pharma/views/manager_bar", TRUE, $content_data); ?>
		
		
		<div id="wrapper">
			
			<!-- Page Main Wrapper -->
			<div class="page-wrapper" id="page-top">

				<!-- Header -->
				<div class="header-wrapper">

					<!-- Header Main Container -->
					<div class="header-main">

						<!-- Container -->
						<div class="container">

							<!-- Main Navigation & Logo -->                    
							<div class="main-navigation">

								<div class="row">

									<!-- Main Navigation -->
									<div class="col-md-12 columns">

										<nav class="navbar navbar-default gfx-mega nav-left" role="navigation">


												<!-- Brand and toggle get grouped for better mobile display -->
												<div class="navbar-header">
													<a class="navbar-toggle" data-toggle="collapse" data-target="#gfx-collapse"></a>
													<div class="logo">
														<a href="{url}"><img src="{url}img/logo.png" alt="Logo"></a>
													</div>
												</div>
												
												<?php $active = $this->config->item('sys_active_page'); ?>

												<!-- Collect the nav links, forms, and other content for toggling -->
												<div class="collapse navbar-collapse" id="gfx-collapse">
													<ul class="nav navbar-nav gfx-nav">
														<li <?php if ( $active == 'pages/home' ) { echo 'class="dropdown active"'; } ?>>
															<a href="{url}">Home</a>
														</li>
														<li <?php if ( $active == 'pages/nieuws' ) { echo 'class="dropdown active"'; } ?>>
															<a href="{url}nieuws">Nieuws</a>
														</li>
														<li <?php if ( $active == 'pages/visie' ) { echo 'class="dropdown active"'; } ?>>
															<a href="{url}visie">Visie</a>
														</li>
														<li <?php if ( $active == 'pages/merken' ) { echo 'class="dropdown active"'; } ?>>
															<a href="{url}merken">Merken</a>
														</li>
														<li <?php if ( $active == 'pages/contact' ) { echo 'class="dropdown active"'; } ?>>
															<a href="{url}contact">Contact</a>
														</li>
													</ul>
												</div><!-- /.navbar-collapse -->

										</nav>

									</div>
									<!-- /Main Navigation -->

								</div>              

							</div>
							<!-- /Main Navigation & Logo -->

						</div>
						<!-- /Container -->

					</div>   
					<!-- /Header Main Container -->

				</div>
				<!-- /Header -->
				
				<div id="content">
					<?php $this->layout->partial($layout_content, TRUE, $content_data); ?>		
				</div>
				
				<!-- Footer -->
				<div class="footer-wrapper">

					<!-- Footer Top -->
					<div class="footer-top">            

						<!-- Google Map -->      	
						<a class="gmap-button">Vind ons op de kaart</a>
						<div class="gmap-wrapper">
							<div id="google-map-footer"></div>
						</div>
						<!-- /Google Map --> 

					</div>
					<!-- /Footer Top -->

					<!-- Footer Bottom -->
					<div class="footer-bottom">

						<div class="container">

							<div class="row">
								<!-- Footer Menu -->
								<div class="col-md-12 col-sm-12 columns">
									<div class="menu-footer">
										<ul class="list-inline">
											<li><a href="{url}">Home</a></li>
											<li><a href="{url}nieuws">Nieuws</a></li>
											<li><a href="{url}visie">Visie</a></li>
											<li><a href="{url}merken">Merken</a></li>                                                                       
											<li><a href="{url}contact">Contact</a></li>
										</ul>
									</div>
								</div>
								<!-- /Footer Menu -->

								<!-- Info -->
								<div class="col-md-12 col-sm-12 columns">
									<div class="copyright">
										<p><:imp_footer_info:></p>
									</div>
								</div>
								<!-- /Info -->

								<!-- Copyright -->
								<div class="col-md-12 col-sm-12 columns">
									<div class="copyright">
										<p>Copyright &COPY; 2015 Imperia bvba - <a href="{url}users/login/">login</a> - Ontwikkeld door <a href="http://www.artosys.be" style="color: #95a252;" target="_blank">Artosys</a></p>
									</div>
								</div>
								<!-- /Copyright -->
							</div>

						</div>

					</div>
					<!-- /Footer Bottom -->

				</div>
				<!-- /Footer -->

			</div>	

			<a href="#page-top" class="scrollup smooth-scroll" ><span class="fa fa-angle-up"></span></a>
			
		</div>
		
		<script type="text/javascript">
			var siteurl = "{url}";
		</script>
		
		<!-- Javascripts
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		

		<script type="text/javascript" src="{url}js/themejs/jquery.countdown.js"></script>

		<!-- Preloader -->
		<script src="{url}js/themejs/jquery.queryloader2.min.js" type="text/javascript"></script>

		<!-- Smooth Scroll -->
		<script src="{url}js/themejs/SmoothScroll.js" type="text/javascript"></script>

		<!-- Stick On Scroll -->
		<script src="{url}js/themejs/jquery.stickOnScroll.js" type="text/javascript"></script>

		<!-- Scrolling Smooth to section - requires jQuery Easing plugin -->
		<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

		<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
		<script src="{url}js/revolutionslider/jquery.themepunch.plugins.min.js"></script>
		<script src="{url}js/revolutionslider/jquery.themepunch.revolution.min.js"></script>          

		<!-- LivIcons -->
		<script src="{url}js/livicons/livicons-1.3.min.js" type="text/javascript"></script>
		<script src="{url}js/livicons/raphael-min.js" type="text/javascript"></script>

		<!-- Portfolio -->
		<script src="{url}js/themejs/jquery.isotope.min.js" type="text/javascript"></script>
		<script src="{url}js/themejs/jquery.colio.min.js" type="text/javascript"></script>

		<!-- Parallax -->
		<script src="{url}js/themejs/jquery.stellar.min.js" type="text/javascript"></script>

		<!-- Carousel -->
		<script src="{url}js/owl-carousel/owl.carousel.js" type="text/javascript"></script>

		<!-- Counters -->
		<script src="{url}js/themejs/jquery.countTo.js" type="text/javascript"></script>

		<!-- Lightbox -->
		<script src="{url}js/themejs/jquery.magnific-popup.min.js" type="text/javascript"></script>

		<!-- Tooltips -->
		<script src="{url}js/themejs/jQuery.Opie.Tooltip.min.js" type="text/javascript"></script>

		<!-- Animation Viewport -->
		<script src="{url}js/themejs/jquery.waypoints.min.js" type="text/javascript"></script>

		<!-- Pie Chart -->
		<script src="{url}js/themejs/jquery.easypiechart.min.js" type="text/javascript"></script>

		<!-- Google Maps -->
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&sensor=false"></script>

		<!-- Load Scripts -->
		<script src="{url}js/themejs/application.js"></script>
		
		
		<?php $this->layout->partial("platforms/pharma/views/manager_footer", TRUE, $content_data); ?>
		
	</body>
</html>